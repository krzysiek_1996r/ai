public class City {

    private int index;
    private double x;
    private double y;

    public City (int index, double x, double y){
        this.index = index;
        this.x = x;
        this.y = y;
    }

    public int getIndex() {
        return index;
    }
    public double getX() {
        return  x;
    }
    public double getY() {
        return y;
    }
    public void setX(double x) {
        this.x = x;
    }
    public void setY(double y) {
        this.y = y;
    }

    public double distance(City city) {
        double deltaX = city.getX() - this.getX();
        double deltaY = city.getY() - this.getY();
        return Math.sqrt(Math.pow(deltaX,2) + Math.pow(deltaY,2));
    }

    public String toString(){
        return getIndex() + "   " + getX() + "  " + getY();
    }

}
