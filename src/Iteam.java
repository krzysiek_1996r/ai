public class Iteam {

    private int index;
    private int profit;
    private int weight;
    private int idCity;

    public Iteam(int id, int profit, int weight, int idCity){
        this.index = id;
        this.profit = profit;
        this.weight = weight;
        this.idCity = idCity;
    }

    public int getIndex() {
        return index;
    }

    public int getProfit() {
        return profit;
    }

    public int getWeight() {
        return weight;
    }

    public int getIdCity() {
        return idCity;
    }

    public String toString() {
        return getIndex() + "   " + getProfit() + " " + getWeight() + "   " + getIdCity();
    }

}
