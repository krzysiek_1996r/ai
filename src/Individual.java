import java.util.ArrayList;
import java.util.Random;

public class Individual {

    private ArrayList<City> individual;

    public Individual(){
        this.individual = new ArrayList<City>();
    }

    public ArrayList<City> getIndividual() {
        return individual;
    }

    public String toString(){
        return getIndividual().toString();//Error???
    }

    public double evaluation (){
       double eval = 0;

       for(int i = 0; i < (getIndividual().size()-1); i++){
           eval += (getIndividual().get(i)).distance(getIndividual().get(i+1));
       }
       eval += (getIndividual().get(getIndividual().size()-1)).distance(getIndividual().get(0));

       return eval;
   }

   public Individual crossover(ArrayList<Individual> parents){
        Individual children = new Individual();
        int sizeOfIndiv = parents.get(0).getIndividual().size();
        int cuttingPlace = new Random().nextInt(sizeOfIndiv);
        for (int i = 0; i < sizeOfIndiv; i++){
            if(i < cuttingPlace)
                children.getIndividual().add(parents.get(0).getIndividual().get(i));
        }


        return children;
   }
//
//   public Individual mutate(ArrayList<Individual> parents){
//        Individual children = new Individual();
//        int sizeOfIndiv = parents.get(0).getIndividual().size();//number of cities in Individual
//        int randCity1 = new Random().nextInt(sizeOfIndiv); //Cities which will be changed
//        int randCity2 = new Random().nextInt(sizeOfIndiv);
//        if (randCity1 < randCity2){
//            for(int i = randCity2; i < randCity2-randCity1-1; i++){
//                children = parents.get(0).getIndividual().
//            }
//        }
//        return children;
//   }



}
