import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class Population {

    private static final int NUMBER_OF_INDIVIDUAL = 100;
    private ArrayList<Individual> population;

    public Population(){
        this.population = new ArrayList<Individual>();
    }

    public Population(ArrayList<Individual> population){
        this.population = population;
    }


    public ArrayList<Individual> getPopulation (){
        return population;
    }


    public ArrayList<String> readData() throws IOException {

        String fileName = "C:/Users/krzys/IdeaProjects/SI-zadanie1/trivial_0.ttp";
        BufferedReader bf = new BufferedReader(new FileReader(fileName));
        ArrayList<String> data = new ArrayList<>();

        String line = bf.readLine();
        while(line != null) {
            data.add(line);
            line = bf.readLine();
        }
        bf.close();

        return data;
    }

    public ArrayList<City> initListOfCities(ArrayList<String> data){

        ArrayList<City>cities = new ArrayList<>();
        String numberOfCities = data.get(2).split("\t")[1];
        int numOfCities = Integer.parseInt(numberOfCities);

        for (int i = 0; i < numOfCities; i++){
            String[] arrOfStr = data.get(10+i).split("\t");
            int id = Integer.parseInt(arrOfStr[0]);
            double x = Double.parseDouble(arrOfStr[1]);
            double y = Double.parseDouble(arrOfStr[2]);
            cities.add(new City(id,x,y));
        }
        return cities;
    }

    public ArrayList<Iteam> initListOfIteams(ArrayList<String> data) {

        ArrayList<Iteam> iteams = new ArrayList<>();
        String numberOfIteams = data.get(3).split("\t")[1];
        int numOfIteams = Integer.parseInt(numberOfIteams);
        String numberOfCities = data.get(2).split("\t")[1];
        int numOfCities = Integer.parseInt(numberOfCities);

        for (int i = 0; i < numOfIteams; i++){
            String[] arrOfStr = data.get(11+numOfCities+i).split("\t");
            int id = Integer.parseInt(arrOfStr[0]);
            int profit = Integer.parseInt(arrOfStr[1]);
            int weight = Integer.parseInt(arrOfStr[2]);
            int idCity = Integer.parseInt((arrOfStr[3]));
            iteams.add(new Iteam(id, profit, weight, idCity));
        }
        return iteams;
    }

    public  ArrayList<Double> evalOfPopulation(){
        ArrayList<Double> evalPop = new ArrayList<>();

        for(int i = 0; i < population.size(); i++){
            evalPop.add(getPopulation().get(i).evaluation());
        }

        return evalPop;
    }

    public Population initPopulation(ArrayList<City> listOfCities){

        Population population = new Population();
        for (int i = 0; i < NUMBER_OF_INDIVIDUAL; i++){
            ArrayList<City> cities = new ArrayList<>(listOfCities);
            Individual individual = new Individual();
            while(cities.size() > 0){
                Random generator = new Random();
                int randomCity = generator.nextInt(cities.size());
                individual.getIndividual().add(cities.get(randomCity));
                cities.remove(randomCity);
            }
            population.getPopulation().add(individual);
        }
        return population;
    }

//Random individual from population
    public Individual random (Population population){
        int number = 5;
        Population copyOfPopulation = new Population(population.getPopulation());
        ArrayList<Individual> randomIndividual = new ArrayList<>();//random 5 (variable number)different individual
        for (int i = 0; i < number; i++){
            int randIndiv = new Random().nextInt(copyOfPopulation.getPopulation().get(0).getIndividual().size());//number of the cities
            randomIndividual.add(copyOfPopulation.getPopulation().get(randIndiv));
            copyOfPopulation.getPopulation().remove(randIndiv);
        }
        double temp = 0;
        Individual theBest = new Individual();
        for(Individual ind : randomIndividual){
            double eval = ind.evaluation();
            if(eval < temp) {
                temp = eval;
                theBest = ind;
            } else
                if(temp == 0) {
                    temp = eval;
                    theBest = ind;
                }
        }
        return theBest;
    }

    public ArrayList<Individual> select(Population population){
        ArrayList<Individual> parents = new ArrayList<>();
        Individual parent1 = random(population);
        Individual parent2 = random(population);
        if(parent1.equals(parent2)){
            select(population);
        } else {
            parents.add(parent1);
            parents.add(parent2);
        }
        return parents;
    }
}
