import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Main {

    public static final int NUMBER_OF_GENERATION = 1000;
    public static final double PROBABILITY_OF_CROSSOVER = 0.9;
    public static final double PROBABILITY_OF_MUTATE = 0.05;


    public static boolean probability(double prob){
        double rand = new Random().nextDouble();
        if (rand < prob) return true;
        else return false;
    }

    public static void main(String[] args) throws IOException {
        Population firstPop = new Population();
        ArrayList<String> data = firstPop.readData();//read data from file
        //List of cities
        ArrayList<City> cities = firstPop.initListOfCities(data);//create list of the cities
        //List of iteams
        ArrayList<Iteam> iteams = firstPop.initListOfIteams(data);
        //First population
        Population pop = firstPop.initPopulation(cities);// create random first population

        System.out.println("Size of population: " + pop.getPopulation().size());

       ArrayList<Double> listOfEval = pop.evalOfPopulation();// create list of eval individuals

//       for (int i = 0; i < NUMBER_OF_GENERATION; i++){
//           Population newPop = new Population();
//           for(int j = 0; j < pop.getPopulation().size(); i++){
//                ArrayList<Individual> parents = firstPop.select(pop);//select 2 individuals
//                if(probability(PROBABILITY_OF_CROSSOVER)){
//                   pop.getPopulation().get(0).crossover(parents);
//                }
//                if(probability(PROBABILITY_OF_MUTATE)){
//
//                }
//               Individual children = new Individual();//
//               newPop.getPopulation().add(children);//add individual to new population
//           }
//
//       }

       Individual theBestSolution;
        Collections.sort(listOfEval);
       for (Double d : listOfEval){
           System.out.println(d);
       }
    }
}
